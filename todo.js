
class Todo {
    constructor(list, config) {
        this.list = Array.isArray(list)
            ? list.filter(item => item.text !== undefined && item.status !== undefined)
            : [];
        this.config = config;
        if (config.onInit && typeof config.onInit === 'function') {
            config.onInit.call(this.list);
        }
    }

    add(text) {
        const { list, config } = this;
        list.unshift({ text, status: false });
        if (config.onAdd && typeof config.onAdd === 'function') {
            config.onAdd.call(list, 0, list[0]);
        }
    }

    edit(id, text) {
        const { list, config } = this;
        if (!list[id] || list[id].text === text) {
            return false;
        }
        list[id].text = text;
        if (config.onChange && typeof config.onChange === 'function') {
            config.onChange.call(list, id, list[id]);
        }
    }

    changeStatus(id, status) {
        const { list, config } = this;
        if (!list[id] || list[id].status === status) {
            return false;
        }
        list[id].status = status;
        const [ item ] = list.splice(id, 1);
        if (status) {
            list.push(item);
        } else {
            list.unshift(item);
        }
        if (config.onChangeStatus && typeof config.onChangeStatus === 'function') {
            config.onChangeStatus.call(list, id, list[id]);
        }
    }

    remove(id) {
        const { list, config } = this;
        if (!list[id]) {
            return false;
        }
        const [ item ] = list.splice(id, 1);
        if (config.onRemove && typeof config.onRemove === 'function') {
            config.onRemove.call(list, id, item);
        }
    }
};