
let todo;
const storge = {
  get() {
    return localStorage.list ? JSON.parse(localStorage.list) : [];
  },
  set(list) {
    localStorage.list = JSON.stringify(list);
  }
};

$(() => {

  const tableRenderer = new TableRenderer({
    wrapper: $('#wrapper'),
    onSave: (id, text) => {
      if (id === 'new') {
        todo.add(text);
      } else {
        todo.edit(id, text);
      }
    },
    onChangeStatus: (id, status) => {
      todo.changeStatus(id, status);
    },
    onRemove(id) {
      todo.remove(id);
    }
  });

  todo = new Todo(storge.get(), {
    onInit() {
      tableRenderer.render(this);
    },

    onAdd() {
      storge.set(this);
      tableRenderer.render(this);
    },

    onChange() {
      storge.set(this);
      tableRenderer.render(this);
    },

    onChangeStatus() {
      storge.set(this);
      tableRenderer.render(this);
    },

    onRemove() {
      storge.set(this);
      tableRenderer.render(this);
    }
  });

  $('.add-new-item').on('click', e => {
    e.preventDefault();
    tableRenderer.add(true);
  });

});
