
class TableRenderer {
  constructor(config) {
    this.list = [];
    this.config = config;
    this.isAdd = false;
    this.isEdit = -1;

    this.config.wrapper.on('click', e => {
      const target = $(e.target);
      const type = target.attr('data-type');
      if (type) {
        switch (type) {
          case 'save':
            this.save(target.attr('data-id'));
            break;
          case 'open':
            this.changeStatus(target.attr('data-id'), false);
            break;
          case 'close':
            this.changeStatus(target.attr('data-id'), true);
            break;
          case 'edit':
            this.edit(target.attr('data-id'));
            break;
          case 'cancel':
            this.cancel();
            break;
          case 'delete':
            this.remove(target.attr('data-id'));
            break;
        }
      }
    });
  }

  add() {
    if (!this.isAdd) {
      this.isAdd = true;
      this.isEdit = -1;
      this.render();
    }
  }

  save(id) {
    const { config } = this;
    const textarea = config.wrapper.find('textarea[name="edit-item"]');
    const text = textarea.val().trim();
    if (!/\w+/.test(text)) {
      textarea.parent().addClass('has-warning');
    } else {
      if (config.onSave && typeof config.onSave === 'function') {
        config.onSave.call(null, id, text);
      }
      if (id === 'new') {
        this.isAdd = false;
      } else {
        this.isEdit = false;
      }
    }
  }

  changeStatus(id, status) {
    const { config } = this;
    if (config.onChangeStatus && typeof config.onChangeStatus === 'function') {
      config.onChangeStatus.call(null, id, status);
    }
  }

  edit(id) {
    id = parseInt(id);
    if (this.isEdit !== id) {
      this.isEdit = id;
      this.isAdd = false;
      this.render();
    }
  }

  cancel() {
    this.isEdit = -1;
    this.isAdd = false;
    this.render();
  }

  remove(id) {
    const { config } = this;
    if (config.onRemove && typeof config.onRemove === 'function') {
      config.onRemove.call(null, id);
    }
  }

  render(newList) {
    if (newList && Array.isArray(newList)) {
      this.list = Object.assign([], newList);
      this.isEdit = -1;
      this.isAdd = false;
    }

    const { list, config, isAdd, isEdit } = this;

    let html = '';

    if (list.length) {
        html = list.map((item, id) => `
          <tr data-type="changeStatus" data-id="${id}">
            <td>
              ${
                item.status
                  ? '<span class="label label-success">Closed</span>'
                  : '<span class="label label-warning">Open</span>'
              }
            </td>
            <td class="${ item.status ? 'closed' : '' }">
            ${
              (isEdit === id)
                ? `<textarea class="form-control" name="edit-item">${item.text}</textarea>`
                : item.text
            }
            </td>
            <td class="buttons">
              ${
                (isEdit === id)
                  ? `
                      <button data-type="save" data-id="${id}" type="button" class="btn btn-info">Save</button>
                      <button data-type="cancel" data-id="${id}" type="button" class="btn btn-danger">Cancel</button>
                    `
                  : `
                      ${
                        item.status
                          ? `<button data-type="open" data-id="${id}" type="button" class="btn btn-default">Open</button>`
                          : `<button data-type="close" data-id="${id}" type="button" class="btn btn-default">Close</button>`
                      }
                      <button data-type="edit" data-id="${id}" type="button" class="btn btn-info">Edit</button>
                      <button data-type="delete" data-id="${id}" type="button" class="btn btn-danger">Delete</button>
                    `
              }
            </td>
          </tr>
        `);
    } else {
      html = '<tr><td colspan="3" class="text-center">Items not found...</td></tr>';
    }

    if (isAdd) {
      html = `<tr>
                <td></td>
                <td>
                  <div class="form-group">
                    <textarea class="form-control" name="edit-item"></textarea>
                  </div>
                </td>
                <td>
                  <button data-type="save" data-id="new" type="button" class="btn btn-info">Save</button>
                  <button data-type="cancel" data-id="new" type="button" class="btn btn-danger">Cancel</button>
                </td>
              </tr>
              ${list.length ? html : ''}`;
    }

    config.wrapper.html(html);
  }
}
